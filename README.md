---
title: 'Colemak布局的实现'
subtitle: '一个极客的键盘布局'
tags: entertainment solution
---


## 前言
大部分同学使用的键盘布局都是QWERTY布局

而科学研究表明,可能这个设计不是最高效率的布局,甚至的有意为了降低打字的效率而研究的，那么当初为什么要这么设计呢？  
关于键盘布局历史故事的详细内容，可以参考：[知乎:键盘布局有哪些种？各种布局的设计出发点是什么？](https://www.zhihu.com/question/20121876/answer/129017959)


**今天小编给大家介绍另一种布局**

## colemak布局

这种键盘布局,根据热力图显示,我们打字中经常使用的按键（比如元音字母a,o,e,i,u）都会被设计在键盘的中间的一排中,这样可以减少我们在打字的过程中的手指的移动,打字的效率自然就会提高

切换这种布局的方式有很多，经过小编汗水亲测以及对比，autohotkey是其中最好的解决方案，因为他是通过脚本进行按键的替换，所以在使用的时候你不需要局限于输入法，你可以使用colemak输入英文，也可以使用它来输入中文（拼音输入）。  
万事开头难，你一开始使用他一定会不适应的，但是只要你坚持打到3个星期，我相信，这个时候你的打字速度足够满足正常的输入需求了。



### colemak windows 输入法 实现方案

[Colemak AutoHotKey](https://gitee.com/victorfengming/colemak_geek/blob/master/windows/README.md)

### colemak linux 输入法 实现方案

[deepin-colemak](https://victorfengming.gitee.io/linux/deepin-colemak/)

### colemak 手机输入法 实现方案

[Colemak Geek](https://gitee.com/victorfengming/colemak_geek/blob/master/Android/README.md)


## 项目源代码地址
码云：https://gitee.com/victorfengming/colemak_geek

感觉有帮助的伙伴可以给小编`star`一下

